<?php
require_once("config.php");

# Database Connection Information

class Database_Connection {
		
	public static function getDBConn($config) {
		$type = $config['db_type'];
		$host = $config['db_host'];
		$dbname = $config['db_name'];
		$user = $config['db_user'];
		$pass = $config['db_pass'];
		$conn_string =
			"$type:host=$host;dbname=$dbname;charset=utf8mb4";
		return new PDO($conn_string, $user, $pass);
	}
}
