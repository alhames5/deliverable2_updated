<?php
	require_once("config.php");

	class TemplateEngine{
		
		private $config;
		
		public function __construct($config) {
			$this->config = $config;
		}
		
		public function template( $name, $args ) {
			$app_root = $this->config["app_root"];
			$file = "$app_root/templates/".$name.".php";
			
			if ( !file_exists( $file )) {
				return 'File not found';
			}
			
			if( is_array( $args )) {
				extract( $args );
			}
			
			ob_start();
			include $file;
			return ob_get_clean();
			
		}
		
	}

	$templateEngine = new TemplateEngine($config);
	