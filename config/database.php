<?php

require_once("config.php");
require_once("Database_Connection.php");

class Database {
	
	private $config;
	
	public function __construct($config) {
		$this->config = $config;
	}
	
	private function dbconn()
	{
		
		$conn = Database_Connection::getDBConn($this->config);
		if(!$conn) throw new Exception("Database Connection Error");
		return $conn;
		
	}
	
	public function getUserByUsername($username) {
		
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL getuserbyusername(:username)");
		$result->bindValue(":username", $username);
		$result->setFetchMode(PDO::FETCH_ASSOC);
		$result->execute();
		$users = $result->fetchAll();
        return $users[0];
	}
	
	public function checkUserExist($username) {
		$conn = $this->dbconn();
		$stmt = $conn->prepare("SELECT COUNT(username) AS userCount 
								FROM users WHERE username = :uniqueuser");
		$stmt->execute(array('uniqueuser' => $username));
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		if ($result['userCount'] != 0) {
			$error = 'NotUniqueUser';
			return $error;
		}
		
		else {
			$error = 'UniqueUser';
			return $error;
		}
			
	}
	
	public function checkEmailExist($email) {
		$conn = $this->dbconn();

		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
				
		//Validate email
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$stmt = $conn->prepare("SELECT COUNT(email) AS EmailCount 
								FROM users WHERE email = :uniqueemail");
			$stmt->execute(array('uniqueemail' => $email));
			#$stmt->bindValue(':uniqueemail', $email);
			$result=$stmt->fetch(PDO::FETCH_ASSOC);
					
			if ($result['EmailCount'] != 0) {
				$error = 'NotUniqueEmail';
				return $error;
			}
			
			else {
				$error = 'UniqueEmail';
				return $error;
			}
		}
		
		else {
			$error = 'NotValidEmail';
			return $error;
		}
		
	}
	
	public function insertNewUser($username, $fname, $lname, $email, $password) {
	
		$conn = $this->dbconn();
		$hash = password_hash($password, PASSWORD_DEFAULT);
		$sql = "INSERT INTO users (username, fname, lname, email, password)
							VALUES (:username, :fname, :lname, :email, :password);";
					
		$query = $conn->prepare($sql);
					
		//bind values to statement
		$query->bindValue(':username', $username);
		$query->bindValue(':fname', $fname);
		$query->bindValue(':lname', $lname);
		$query->bindValue(':email', $email);
		$query->bindValue(':password', $hash);
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$query->execute();
	}


//deleteuser(user_id)

	public function deleteuser($userID) {
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL deleteuser(:user_id)");
		$result->bindValue(":user_id", $userID);
		$result->execute();
		
	}

//list users

	public function listUsers() {
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL listusers()");
		$result->setFetchMode(PDO::FETCH_ASSOC);
		$result->execute();
		$users = $result->fetchAll();
		return $users;
	}

//updateuser

	public function updateUser($userID, $username, $fname, $lname, $email) {
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL updateUser(:user_id, :username, :fname, :lname, :email)");
		$result->bindValue(":user_id", $userID);
		$result->bindValue(":username", $username);
		$result->bindValue(":fname", $fname);
		$result->bindValue(":lname", $lname);
		$result->bindValue(":email", $email);
		$result->execute();
	}

//updateuserlogintime

	public function updateUserLoginTime($userID) {
		$conn = $this->dbconn();
                $result = $conn->prepare("CALL updateuserlogintime(:user_id)");
                $result->bindValue(":user_id", $userID);
                $result->execute();
	}

//updateuserpassword

	public function updateUserPassword($userID, $password) {
		$conn = $this->dbconn();
                $result = $conn->prepare("CALL updateuserpassword(:user_id, :password)");
		$result->bindValue(":user_id", $userID);
		$result->bindValue(":password", $password);
                $result->execute();
	}
	
	public function getStylesByUserID($userID) {
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL getstylesbyuserid(:user_id)");
		$result->bindValue(":user_id", $userID);
		$result->setFetchMode(PDO::FETCH_ASSOC);
		$result->execute();
		$styles = $result->fetchAll();
		return $styles;
	}

	public function insertStyle($userID, $selectorID, $value) {
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL insertstyle(:user_id, :selector_id, :value)");
		$result->bindValue(":user_id", $userID);
		$result->bindValue(":selector_id", $selectorID);
		$result->bindValue(":value", $value);
		$result->execute();
	}

	public function updateStyle($userID, $selectorID, $value) {
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL updatestyle(:user_id, :selector_id, :value)");
		$result->bindValue(":user_id", $userID);
                $result->bindValue(":selector_id", $selectorID);
                $result->bindValue(":value", $value);
		$result->execute();
	}

	public function deleteStyle($userID, $selectorID) {
                $conn = $this->dbconn();
                $result = $conn->prepare("CALL deletestyle(:user_id, :selector_id)");
                $result->bindValue(":user_id", $userID);
                $result->bindValue(":selector_id", $selectorID);
                $result->execute();
	}

	public function getSelectors() {
		$conn = $this->dbconn();
		$result = $conn->prepare("CALL getselectors()");
		$result->setFetchMode(PDO::FETCH_ASSOC);
		$result->execute();
		return $result->fetchAll();
	}

	public function insertSelector($name) {
                $conn = $this->dbconn();
                $result = $conn->prepare("CALL insertselector(:name)");
                $result->bindValue(":name", $name);
                $result->execute();
	}

	public function deleteSelector($selectorID) {
                $conn = $this->dbconn();
                $result = $conn->prepare("CALL deleteselector(:selector_id)");
                $result->bindValue(":selector_id", $selectorID);
                $result->execute();
	}
	
	public function insertImage($image, $userID) {
		$conn = $this->dbconn();
        $result = $conn->prepare("CALL insertimage(:image, :userID)");
		$result->bindValue(":image", $image);
		$result->bindValue(":userID", $userID);
		$result->execute();
		
	}
	
	public function getImage($userID) {
		$conn = $this->dbconn();
        $result = $conn->prepare("CALL getimage(:user_id)");
		$result->bindValue(":user_id", $userID);
		$result->setFetchMode(PDO::FETCH_ASSOC);
		$result->execute();
		$images = $result->fetchAll();
		return $images[0];
	}
}


$database = new database($config);

	
	
