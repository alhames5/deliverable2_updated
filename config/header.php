<?php
require_once("Database.php");
require_once("config.php");
global $database;
global $config;
?>

<?php
if(!isset($_SESSION)) 
	session_start()
?>

<html>
	<head>
		<title>Business Management Website</title>
		<link href="public/assets/styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<h1>Business Operation Management System</h1>
			<ul class="topnav">
				<li><a class="active" href="../index.php">Home Page</a></li>
				<li><a href="../Contact.php">Contact</a></li>
				<li><a href="../about.php">About</a></li>		
<?php

if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"]) {
	if (isset($_SESSION["user"]) && $_SESSION["user"]["role_name"] == "admin") {
		 
?>
				<li><a href="public/admin.php">Admin Console</a></li>
<?php
	}
?>  
				<li><a href="public/logout.php">Logout</a></li>
<?php
} else {
?>
				<li><a href="../public/login_form.php">Associate Login</a></li>
<?php
}
?>
				</ul>
			

		