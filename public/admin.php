<?php
	require_once("../config/database.php");
	require_once("../config/templateEngine.php");
	session_start();
?>
<head>
		<title>Business Management Website</title>
		<link href="assets/styles.css" rel="stylesheet" type="text/css">
</head>
<?php 
	if (!isset($_SESSION["user"]) || $_SESSION["user"]["role_name"] != "admin") {
		echo "<script>window.location.replace('/');</script>";
	} else {
		$users = $database->listusers();
		$args = array('users' => $users);
		
		include("../config/header.php");
		
		echo $templateEngine->template("admin", $args);
	}


?>

