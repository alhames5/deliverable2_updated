<?php
session_start();

include("../config/header.php");

?>
<!DOCTYPE HTML>
<head>
<title>User Registration Page</title>
<link href="assets/styles.css" rel="stylesheet" type="text/css">
</head>
<body>


<form class="login-input" method="POST" action="user_registration.php">
    <div class="body"></div>
    <div class="login">
	<h1>Welcome Managers, Please SignUp Below</h1>
	<input type="text" name="username" class="login-input" placeholder="Pick a user name" autofocus > <br><br>
    <input type="text" name="fname" class="login-input" placeholder="First Name"  > <br><br>
    <input type="text" name="lname" class="login-input" placeholder="Last Name" > <br><br>
	<input type="text" name="email" class="login-input" placeholder="Email Address" > <br>
	<input type="password" name="password" class="login-input" placeholder="Password" > <br>
	<input type="password" name="repassword" class="login-input" placeholder="Re-type Password" > <br>
    <input type="submit" value="Sign Up" class="login-input">
	
	<?php

	if (!isset($_GET['signup'])) {
			exit();
			}
	else {
		$signupCheck = $_GET['signup'];
		if ($signupCheck == "empty") {
				echo "<p align='center' style='color:red; font-size:150%'>Please provide all the information!</p>";
				exit();
			}
		else if ($signupCheck == "email") {
			echo "<p align='center' style='color:red; font-size:150%'> The email address you have provided is invalid.</p>";
			exit();
		}
		
		else if ($signupCheck == "emailunique") {
			echo "<p align='center' style='color:red; font-size:150%'> Your email already appears to be in the record</p>";
			exit();
		}
		
		else if ($signupCheck == "userunique") {
			echo "<p align='center' style='color:red; font-size:150%'> Username is already taken!</p>";
			exit();
		}
	}
?>
	
    </div>
    </div>
	
</form>
</body>
</html>



