<?php
	session_start();
	require_once '../config/database.php';
	require_once '../config/config.php';
?>

<?php
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		header("Location:user_registration.php");
	}
	
	else if ($_SERVER["REQUEST_METHOD"] == "POST") {
		
		$username = $_POST["username"];
		$fname = $_POST["fname"];
		$lname = $_POST["lname"];
		$email = $_POST["email"];
		$password = $_POST["password"];
		$hash = password_hash($password, PASSWORD_DEFAULT);
		
		if (empty($username) || empty($fname) || empty($lname) || empty($email) || empty($password)) {
			header("Location:signup_associates.php?signup=empty");
			exit();
		}
			
			
		else {			
			$database->insertNewUser($username, $fname, $lname, $email, $password);
			header("Location:signup_associates.php?signup=registered");

			}
	}
					
				
	
?>