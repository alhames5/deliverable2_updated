<?php
require_once("../config/config.php");
require_once($config["app_root"]."/config/Database.php");
require_once($config["app_root"]."/config/TemplateEngine.php");
session_start();


if ($_SERVER["REQUEST_METHOD"] == "GET") {

	include($config["app_root"]."/config/header.php");

	$selectors = $database->getSelectors();

	$args = array("selectors" => $selectors);

	echo $templateEngine->template("edit-css-theme", $args);

	//include($config["app_root"]."/config/footer.php");
} else if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$user = $_SESSION["user"];
	$selectorID = $_POST["selector_id"];
	$cssValue = $_POST["css-value"];
	$database->insertStyle($user["user_id"], $selectorID, $cssValue);

	header("Location: /edit-css.php");
}
	

