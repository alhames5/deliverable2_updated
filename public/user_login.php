<?php

	session_start();
	require_once '../config/database.php';
	require_once '../config/config.php';
	global $config;
	global $database;
	
	if($_SERVER["REQUEST_METHOD"] == "GET") {
		
		header("Location: ../");
	}
	
	else if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
		$username = $_POST['username'];
		$password = $_POST['password'];
		$hash = password_hash($password, PASSWORD_DEFAULT);
		
		
		if (empty($username) || empty($password)) {
			header("Location:login_form.php?signin=empty");
			exit();
		}
		
		else { 
				$user = $database->getUserByUsername($username);
				$hash_pwd = $user['password'];
				$hash = password_verify($password, $hash_pwd);
				
				if($hash == false) {
					header("location: login_form.php?signin=invalid");
				}
				else {
					$_SESSION["user"] = $user;
					$_SESSION["logged_in"] = true;
					header("Location: ../");	
				}
		}
	}
			
?>