<head>
		<link href="assets/styles.css" rel="stylesheet" type="text/css">
</head>

<?php
	require_once("../config/database.php");
	require_once("../config/templateEngine.php");
	session_start();
  
	include("../config/header.php");

	
	$args = array();
	echo $templateEngine->template("loginForm", $args);
	
?>

<?php
	
	if (!isset($_GET['signin'])) {
			exit();
			}
	else {
		$signinCheck = $_GET['signin'];
		if ($signinCheck == "empty") {
				echo "<p align='center' style='color:red'>Username and/or Password cannot be blank!</p>";
				exit();
			}
			
		if ($signinCheck == "invalid") {
				echo "<p align='center' style='color:red'>Incorrect Username/Password Combination!</p>";
				exit();
		}
	}
?>
