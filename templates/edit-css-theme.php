<h1>Edit your CSS theme!</h1>

<form method="post" action="../edit-css.php">
	<select name="selector_id">
	<?php foreach($selectors as $selector) {
		$selectorID = $selector["selector_id"];
		$selectorName = $selector["name"];
		echo "<option value='$selectorID'>$selectorName</option>\n";
	}
?>
	</select>
	<input type="text" name="css-value" />
	<input type="submit" name="submit" value="Submit" />
</form>
